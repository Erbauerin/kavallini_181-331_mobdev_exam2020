import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
//import QtMultimedia 5.12
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1


ApplicationWindow {
    id: general_Window // идентификатор окна(?) должен начинаться с МАЛЕНЬКОЙ буквы!!!
    // если к объекту не планируется обращаться, то id можно не задавать
    signal signalMakeRequestHTTP();
    visible: true
    width: 320 // ширина окна
    height: 480 // высота окна
    title: qsTr("Главное окно") // имя окошка, только в кавычках можно писать по-русски
    //qsTr - функция для переключения языка строки (если два языка, то нужно предоставить две строки)
    Material.accent: "#16bf62" //насыщенно-зелёный
    Material.background: "#000000" // чёрный

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page{
            id: page1
        }
    }

    Drawer{
        id: drawer
        width: 0.45 * parent.width
        height: parent.height

        GridLayout{
            width: parent.width
            columns: 1

            Button{
                text: "Страница 1"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 0
                    drawer.close()
                }
            }
        }
    }

}
